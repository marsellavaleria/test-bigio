<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'LoginController@index');
Route::get('/login', 'LoginController@index')->name('login');
Route::post('/login', 'LoginController@do_login')->name('logins.login');
Route::get('/logout', 'LoginController@do_logout')->name('logins.logout');
Route::resource('home','DashboardController');
Route::get('/guru','GuruController@index');
Route::get('/guru/create','GuruController@create');
Route::post('/guru/store','GuruController@store');
Route::get('/guru/edit/{id}','GuruController@edit');
Route::patch('/guru/update/{id}','GuruController@update');
Route::get('/guru/show/{id}','GuruController@show');
Route::delete('/guru/delete', 'GuruController@destroy');
Route::get('/murid','MuridController@index');
Route::get('/murid/create','MuridController@create');
Route::post('/murid/store','MuridController@store');
Route::get('/murid/edit/{id}','MuridController@edit');
Route::patch('/murid/update/{id}','MuridController@update');
Route::get('/murid/show/{id}','MuridController@show');
Route::delete('/murid/delete', 'MuridController@destroy');
Route::get('/nilaimurid','NilaiMuridController@index');
Route::get('/nilaimurid/create','NilaiMuridController@create');
Route::post('/nilaimurid/store','NilaiMuridController@store');
Route::get('/nilaimurid/edit/{id}','NilaiMuridController@edit');
Route::patch('/nilaimurid/update/{id}','NilaiMuridController@update');
Route::get('/nilaimurid/show/{id}','NilaiMuridController@show');
Route::delete('/nilaimurid/delete', 'NilaiMuridController@destroy');
Route::get('/lihatnilaimurid/{id}','NilaiMuridController@showNilaiPerMurid');
