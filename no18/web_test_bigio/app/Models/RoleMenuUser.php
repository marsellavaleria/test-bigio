<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleMenuUser extends Model
{
    protected $table = 'roleusermenuviews';
}
