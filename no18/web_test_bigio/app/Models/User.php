<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    protected $table = "user";
    use SoftDeletes;
    protected $fillable = ['username','password','id_admin','id_guru','id_murid'];
}
