<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Murid extends Model
{
    protected $table = 'murid';
    protected $fillable = ['kode','nama','jenis_kelamin','alamat','no_telepon','foto'];
}
