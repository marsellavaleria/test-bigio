<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Guru extends Model
{
    protected $table = 'guru';

    use SoftDeletes;
    protected $fillable = ['code','nama','jenis_kelamin','alamat','no_telepon','foto'];
}
