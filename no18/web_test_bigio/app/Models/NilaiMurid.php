<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NilaiMurid extends Model
{
    protected $table = 'nilai_murid';
    protected $fillable = ['mata_pelajaran','nilai','id_murid'];
}
