<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\NilaiMurid;
use App\Models\NilaiMuridView;
use App\Models\Murid;

class NilaiMuridController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nilaimurids = NilaiMuridView::paginate(10);
        return view('pages.nilaimurid.index')->with(['nilaimurids'=>$nilaimurids]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $murids = Murid::all();
        return view('pages.nilaimurid.create')->with(['murids'=>$murids]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'student'=>'required',
            'subject' => 'required',
            'grade' => 'required']);
        
        NilaiMurid::create([
            'mata_pelajaran' => $request->subject,
            'nilai' => $request ->grade,
            'id_murid' => $request->student
        ]);
        
        return redirect('/nilaimurid');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $nilaimurid = NilaiMuridView::findOrFail($id);
        return view('pages.nilaimurid.show')->with(['nilaimurid'=>$nilaimurid]);
    }

    public function showNilaiPerMurid($id){
        $murid = Murid::where('id',$id)->first();
        $nilaimurids = NilaiMuridView::where('id_murid',$id)->paginate(10);
        return view('pages.lihatnilaimurid.index')->with(['nilaimurids'=>$nilaimurids,'murid'=>$murid]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $nilaimurid = NilaiMurid::findOrFail($id);
        $murids = Murid::all();
        return view('pages.nilaimurid.edit')->with(['nilaimurid'=>$nilaimurid,'murids'=>$murids]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $request->validate([
            'student'=>'required',
            'subject' => 'required',
            'grade' => 'required']);

            NilaiMurid::where('id',$id)
            ->update([
                'mata_pelajaran'=>$request->subject,
                'nilai' => $request->grade,
                'id_murid' => $request ->student
            ]);
        
        return redirect('/nilaimurid');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
