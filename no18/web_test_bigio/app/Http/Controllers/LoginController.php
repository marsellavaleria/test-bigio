<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\RoleMenuUser;
use App\Models\UserView;
use Hash;

class LoginController extends Controller
{
    public function index(){
        return view('pages.login');
    }

    public function do_login(Request $request)
    {   
        $request->validate([
            'username'=>'required',
            'password'=>'required']);
        
            if($this->checkAccount($request->username,$request->password)!=null){
                $user = $this->checkAccount($request->username,$request->password);
                $request -> session()->put('user',$user);
                $menu = RoleMenuUser::where('id_user',$user->iduser)->get();
                $request->session()->put('menu',$menu);
                return redirect()->route('home.index');
            }
            else{
                return redirect()->back()->with('error','Username dan Password salah!');
            } 
    }

    private function checkAccount($username,$password){
        
        $user = UserView:: where ('username',$username)->first();
        if($user!=null){
            if(Hash::check($password,$user['password'])){
                return $user;
            }
        }
        return null;
    }

    public function do_logout(){

        session()->flush("user");

        return redirect("/login");
    }
}
