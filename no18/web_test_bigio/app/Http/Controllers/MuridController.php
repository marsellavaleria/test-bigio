<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Murid;
use App\Models\User;
use App\Models\MuridView;
use Hash;

class MuridController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $murids = Murid::paginate(10);
        return view('pages.murid.index')->with(['murids'=>$murids]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.murid.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'gender' => 'required',
            'address' => 'required',
            'phone_number' => 'required|numeric',
            'username'=>'required',
            'password'=>'required',
            'class'=>'required']);

            $jumlahMurid = Murid::count();
            $code = "M0000".($jumlahMurid+1);
            $jumlahUser = User::count();
            $codeUser = "U0000".($jumlahUser+1);
            $image_name=null;
            $path_image = null;
            if ($request->hasFile('photo')) {
                $path_image = "/img_murid/";
                $image = $request->file('photo');
                $image_name = $code."-".$image->getClientOriginalExtension();
                $destinationPath = public_path('/img_murid');
                $image->move($destinationPath, $image_name);
                $path_image .=$image_name;
            }
            else{
                $path_image = "/img_murid/";
                $image_name="profile.jpg";
                $path_image .=$image_name;
            }

            $murid = new Murid;
            $murid->kode = $code;
            $murid->nama = $request->name;
            $murid->jenis_kelamin = $request->gender;
            $murid->alamat = $request ->address;
            $murid->no_telepon = $request->phone_number;
            $murid->foto = $path_image;
            $murid->kelas = $request->class;
            $murid->save();

            User::create([
                'code' => $codeUser,
                'username' => $request ->username,
                'password' => Hash::make($request->password),
                'id_murid' => $murid->id
            ]);
        
        //  Alert::success('BERHASIL', 'Data Guru Berhasil Ditambahkan!');
         return redirect('/murid');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $murid = MuridView::findOrFail($id);
        return view('pages.murid.show')->with(['murid'=>$murid]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $murid = MuridView::findOrFail($id);

        return view('pages.murid.edit')->with(['murid'=>$murid]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->password_checkbox=='true'){
            $request->validate([
            'name'=>'required',
            'gender' => 'required',
            'address' => 'required',
            'phone_number' => 'required|numeric',
            'username' => 'required',
            'password' =>'required',
            'class'=>'required']);
        }
        else{
            $request->validate([
            'name'=>'required',
            'gender' => 'required',
            'address' => 'required',
            'phone_number' => 'required|numeric',
            'username' => 'required',
            'class'=>'required']);
        }

        $guru = Murid::where('id',$id)->first();
            $image_name=null;
            if ($request->hasFile('photo')) {
                $image = $request->file('photo');
                $image_name = $guru->code.".".$image->getClientOriginalExtension();
                $destinationPath = public_path('/img_guru');
                $image->move($destinationPath, $image_name);
                $path_image = "/img_murid/".$image_name;
            }
            else{
                $image_name="/img_murid/profile.jpg";
                $path_image = "/img_murid/".$image_name;
            }

            Murid::where('id',$id)
            ->update([
                'nama'=>$request->name,
                'jenis_kelamin' => $request->gender,
                'alamat' => $request ->address,
                'no_telepon' => $request->phone_number,
                'foto' => $path_image,
                'kelas'=>$request->class
            ]);

            $muridview = MuridView::where('id',$id)->first();

            User::where('iduser',$muridview->iduser)
            ->update([
                'username'=>$request->username,
                'password'=>Hash::make($request->password)
            ]);

            // Alert::success('BERHASIL', 'Data Admin Berhasil Diubah!');
            return redirect('/murid');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
