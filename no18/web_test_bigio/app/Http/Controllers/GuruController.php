<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use App\Models\Guru;
use App\Models\User;
use App\Models\GuruView;
use Hash;
use Alert;


class GuruController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function index(){
        $gurus = Guru::paginate(10);
        return view('pages.guru.index')->with(['gurus'=>$gurus]);
    }
 
    public function show($id){
        $guru = GuruView::findOrFail($id);
        return view('pages.guru.show')->with(['guru'=>$guru]);
    }

    public function search(Request $request){
		$cari = $request->cari;
		$admins = Guru::where(DB::raw('concat(code,nama)') , 'LIKE' , "%".$cari."%")->paginate(14);
		return view('pages.guru.index',['gurus' => $admins]);
	}

    public function create()
    {
        return view('pages.guru.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {     
        $request->validate([
            'name'=>'required',
            'gender' => 'required',
            'address' => 'required',
            'phone_number' => 'required|numeric',
            'username'=>'required',
            'password'=>'required',
            'subject'=>'required']);

            $jumlahGuru = Guru::count();
            $code = "G0000".($jumlahGuru+1);
            $jumlahUser = User::count();
            $codeUser = "U0000".($jumlahUser+1);
            $image_name=null;
            $path_image = null;
            if ($request->hasFile('photo')) {
                $path_image = "/img_guru/";
                $image = $request->file('photo');
                $image_name = $code."-".$image->getClientOriginalExtension();
                $destinationPath = public_path('/img_guru');
                $image->move($destinationPath, $image_name);
                $path_image .=$image_name;
            }
            else{
                $path_image = "/img_guru/";
                $image_name="profile.jpg";
                $path_image .=$image_name;
            }

            $guru = new Guru;
            $guru->code = $code;
            $guru->nama = $request->name;
            $guru->jenis_kelamin = $request->gender;
            $guru->alamat = $request ->address;
            $guru->no_telepon = $request->phone_number;
            $guru->foto = $path_image;
            $guru->mata_pelajaran = $request->subject;
            $guru->save();

            User::create([
                'code' => $codeUser,
                'username' => $request ->username,
                'password' => Hash::make($request->password),
                'id_guru' => $guru->id
            ]);
        
        //  Alert::success('BERHASIL', 'Data Guru Berhasil Ditambahkan!');
         return redirect('/guru');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $guru = GuruView::findOrFail($id);

        return view('pages.guru.edit')->with(['guru'=>$guru]);
    }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    public function update(Request $request, $id)
    {
        if($request->password_checkbox=='true'){
            $request->validate([
            'name'=>'required',
            'gender' => 'required',
            'address' => 'required',
            'phone_number' => 'required|numeric',
            'username' => 'required',
            'password' =>'required']);
        }
        else{
            $request->validate([
            'name'=>'required',
            'gender' => 'required',
            'address' => 'required',
            'phone_number' => 'required|numeric',
            'username' => 'required']);
        }

        $guru = Guru::where('id',$id)->first();
            $image_name=null;
            if ($request->hasFile('photo')) {
                $image = $request->file('photo');
                $image_name = $guru->code.".".$image->getClientOriginalExtension();
                $destinationPath = public_path('/img_guru');
                $image->move($destinationPath, $image_name);
                $path_image = "/img_guru/".$image_name;
            }
            else{
                $image_name="/img_guru/profile.jpg";
                $path_image = "/img_guru/".$image_name;
            }

            Guru::where('id',$id)
            ->update([
                'nama'=>$request->name,
                'jenis_kelamin' => $request->gender,
                'alamat' => $request ->address,
                'no_telepon' => $request->phone_number,
                'foto' => $path_image,
                'mata_pelajaran'=>$request->subject
            ]);

            $guruview = GuruView::where('id',$id)->first();

            User::where('iduser',$guruview->iduser)
            ->update([
                'username'=>$request->username,
                'password'=>Hash::make($request->password)
            ]);

            // Alert::success('BERHASIL', 'Data Admin Berhasil Diubah!');
            return redirect('/guru');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {   
        Guru::destroy($request->id);
        // Alert::success('BERHASIL', 'Data Admin Berhasil Dihapus!');
        return  response()->json(['success' => true]);
    }
}
