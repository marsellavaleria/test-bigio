
<div class="row g-0">
    <div class="col-sm-3 m-3">
        <img src="{{url($murid->foto)}}" alt="profile" width="200" height="300">
    </div>
    <div class="col-sm-8 ms-3">
        <h5 class="card-title">{{$murid->kode}}- {{$murid->nama}}</h5>
        <table class="table table-borderless">
            <tr>
                <td>ID</td>
                <td>{{$murid->kode}}</td>
            </tr>
            <tr>
                <td>Nama</td>
                <td>{{$murid->nama}}</td>
            </tr>
            <tr>
                <td>Alamat</td> 
                <td>{{$murid->alamat}}</td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                @if ($murid->jenis_kelamin == 'F')
                <td>{{'Wanita'}}</td>
                @else
                <td>{{'Pria'}}</td>
                @endif
            </tr>
            <tr>
                <td>No Telepon</td>
                <td>{{$murid->no_telepon}}</td>
            </tr>
            <tr>
                <td>Username</td>
                <td>{{$murid->username}}</td>
            </tr>
        </table>
    </div>
</div>    
