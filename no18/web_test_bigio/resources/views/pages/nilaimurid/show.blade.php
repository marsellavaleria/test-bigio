
<div class="row g-0">
    <div class="col-sm-8 ms-3">
        <h5 class="card-title">{{$nilaimurid->kode}}- {{$nilaimurid->nama}}</h5>
        <table class="table table-borderless">
            <tr>
                <td>ID</td>
                <td>{{$nilaimurid->kode}}</td>
            </tr>
            <tr>
                <td>Nama</td>
                <td>{{$nilaimurid->nama}}</td>
            </tr>
            <tr>
                <td>Kelas</td> 
                <td>{{$nilaimurid->kelas}}</td>
            </tr>
            <tr>
                <td>Mata Pelajaran</td>
                <td>{{$nilaimurid->mata_pelajaran}}</td>
            </tr>
            <tr>
                <td>Nilai</td>
                <td>{{number_format($nilaimurid->nilai)}}</td>
            </tr>
        </table>
    </div>
</div>    
