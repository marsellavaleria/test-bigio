
  @extends('layouts.index')

  @section('title','Tambah Data Nilai Murid')

  @section('container')
    <script>
        var readURL= function(event) {
            var input = event.target;

            var reader = new FileReader();
            reader.onload = function(){
            var dataURL = reader.result;
            var output = document.getElementById('output');
            output.src = dataURL;
            };
            reader.readAsDataURL(input.files[0]);
        };
    </script>
    <div class="container-fluid">
        <h5 class="title-page pt-3">Form Nilai Murid</h5>
        <nav class="breadcrumb-nav" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <p>Form Nilai Murid</p>
                <li class="breadcrumb-item active"><a href="{{route('home.index')}}">Home</a></li>
                <li class="breadcrumb-item active"><a href="{{url('/nilaimurid')}}">Data Nilai Murid</a></li>
                <li class="breadcrumb-item active me-5" aria-current="page">Tambah</li>
            </ol>
        </nav>
        <div class="card show-card">
        <div class="card-header">
            <div class="row">
                <div class="col-6 mt-1">
                    <strong> Tambah Data Nilai Murid</strong>
                </div>
                <div class="col-6">
                    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                        <a class="btn btn-secondary me-md-2" type="button" href="{{url('/nilaimurid')}}">Kembali</a>
                    </div>
                </div>
            </div>    
        </div>
        <div class="row g-0">
            <div class="col-sm-8 offset-sm-2">
                <div class="card-body">
                    <form method="post" action="{{url('/nilaimurid/store')}}" id="contactForm" enctype="multipart/form-data" class="row gy-2 gx-3 mt-2">
                        @csrf   
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="nama" class="form-label label-required">Murid</label>
                                <select class="form-select @error('student') is-invalid @enderror" name="student"> 
                                    <option value="" selected>-- Pilih Salah Satu --</option>
                                    @foreach($murids as $murid)
                                    <option value="{{$murid->id}}">{{$murid->kode}}-{{$murid->nama}}-{{$murid->kelas}}</option>
                                    @endforeach
                                </select>
                                @error('student')
                                    <div class="invalid-feedback">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="phone" class="form-label label-required">Mata Pelajaran</label>
                                <input type="text" class="form-control @error('subject') is-invalid @enderror" name="subject" id="exampleInputEmail1" placeholder="Masukkan Mata Pelajaran" value="{{old('subject')}}">
                                @error('subject')
                                    <div class="invalid-feedback">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="username" class="form-label label-required">Nilai</label>
                                <input type="number" class="form-control @error('grade') is-invalid @enderror" name="grade" id="exampleInputEmail1" placeholder="Masukkan Nilai" value="{{old('grade')}}">
                                @error('grade')
                                    <div class="invalid-feedback">{{$message}}</div>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary">Simpan</button> 
                        </div>
                    </form> 
                    <p class="fst-italic ms-3 mt-2 text-danger"><strong>Catatan :</strong> (*) Wajib Diisi</p>
                </div>
            </div>
        </div>
    </div>
    @endsection
