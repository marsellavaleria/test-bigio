@extends('layouts.index')

  @section('title','Data Nilai Murid')

  @section('container')
    <div class="container-fluid">
        <h5 class="title-page pt-3">Data Nilai Murid</h5>
        <nav class="breadcrumb-nav" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <p>Data Nilai Murid</p>
                <li class="breadcrumb-item active"><a href="{{route('home.index')}}">Home</a></li>
                <li class="breadcrumb-item" aria-current="page">Data Nilai Murid</li>
            </ol>
        </nav>

        <div class="card show-card">
            <!-- START HEADER -->
            <div class="card-header">
                <div class="row">
                    <div class="col-6 mt-1">
                        <strong> Data Nilai Murid</strong>
                    </div>
                    <div class="col-6">
                        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                            <a class="btn btn-outline-light me-md-2" type="button" href="{{url('/nilaimurid/create')}}">Tambah Data</a>
                        </div>
                    </div>
                </div>    
            </div>
            <!-- END HEADER -->
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Kode</th>
                            <th>Murid</th>
                            <th>Kelas</th>
                            <th>Mata Pelajaran</th>
                            <th>Nilai</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($nilaimurids as $nilaimurid)
                        <tr>
                            <th scope="row">{{$loop->iteration+$nilaimurids->firstItem()-1}}</th>
                            <td>{{$nilaimurid->kode}}</td>
                            <td>{{$nilaimurid->nama}}</td>
                            <td>{{$nilaimurid->kelas}}</td>
                            <td>{{$nilaimurid->mata_pelajaran}}</td>
                            <td>{{number_format($nilaimurid->nilai, 0, ',', '.')}}</td>
                            <td>
                                <a class="btn btn-secondary btn-sm" data-bs-toggle="modal" href="#showDataNilaiMurid" 
                                    data-remote="{{url('/nilaimurid/show',$nilaimurid->id)}}" 
                                    data-toggle="modal"
                                    data-target="#showDataNilaiMurid"
                                    data-title="Detail Nilai Murid"
                                    class="btn btn-info btn-sm">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye-fill" viewBox="0 0 16 16">
                                        <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
                                        <path d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
                                    </svg>
                                </a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="6" class="text-center p-5">
                                Data tidak tersedia
                            </td>
                        </tr>
                        @endforelse
                         <!-- END TABLE -->
                    </tbody>        
                </table>
            </div>
            <div class="card-footer">
                <!-- PAGINATION -->
                <div class="row">
                    <div class="col-sm-4">
                        <p class="text-sm-start fs-6 fw-bold">Halaman {{$nilaimurids->currentPage()}} dari {{$nilaimurids->lastPage()}} | Data {{$nilaimurids->firstItem()}}-{{$nilaimurids->lastItem()}} dari total {{$nilaimurids->total()}}</p>
                    </div>
                    <div class="col-sm-8">
                        <nav class="pagination-admin" aria-label="...">
                            <ul class="pagination d-flex justify-content-end">
                                <li class="page-item {{$nilaimurids->currentPage() == $nilaimurids->onFirstPage() ? 'disabled': ''}}">
                                    <a class="page-link" href="{{$nilaimurids->url($nilaimurids->onFirstPage())}}" id="FirstPage">Previous</a>
                                </li>
                                {{$nilaimurids->appends(Request::all())->onEachSide(1)->links()}}
                                <li class="page-item {{$nilaimurids->currentPage() == $nilaimurids->lastPage() ? 'disabled': ''}}">
                                    <a class="page-link" href="{{$nilaimurids->url($nilaimurids->lastpage())}}" id="LastPage">Last</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- END PAGINATION -->
            </div>
        </div> 
    </div>
    @endsection    

   