@extends('layouts.index')

  @section('title','Data Guru')

  @section('container')
    <div class="container-fluid">
        <h5 class="title-page pt-3">Data Guru</h5>
        <nav class="breadcrumb-nav" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <p>Data Guru</p>
                <li class="breadcrumb-item active"><a href="{{route('home.index')}}">Home</a></li>
                <li class="breadcrumb-item" aria-current="page">Data Guru</li>
            </ol>
        </nav>

        <div class="card show-card">
            <!-- START HEADER -->
            <div class="card-header">
                <div class="row">
                    <div class="col-6 mt-1">
                        <strong> Data Guru</strong>
                    </div>
                    <div class="col-6">
                        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                            <a class="btn btn-outline-light me-md-2" type="button" href="{{url('/guru/create')}}">Tambah Data</a>
                        </div>
                    </div>
                </div>    
            </div>
            <!-- END HEADER -->
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">ID</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Mata Pelajaran</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($gurus as $guru)
                        <tr>
                            <th scope="row">{{$loop->iteration+$gurus->firstItem()-1}}</th>
                            <td>{{$guru->code}}</td>
                            <td>{{$guru->nama}}</td>
                            <td>{{$guru->mata_pelajaran}}</td>
                            <td>
                                <a class="btn btn-secondary btn-sm" data-bs-toggle="modal" href="#showDataGuru" 
                                    data-remote="{{url('/guru/show',$guru->id)}}" 
                                    data-toggle="modal"
                                    data-target="#showDataGuru"
                                    data-title="Detail Guru {{$guru->code}}"
                                    class="btn btn-info btn-sm">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-eye-fill" viewBox="0 0 16 16">
                                        <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
                                        <path d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
                                    </svg>
                                </a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="6" class="text-center p-5">
                                Data tidak tersedia
                            </td>
                        </tr>
                        @endforelse
                         <!-- END TABLE -->
                    </tbody>        
                </table>
            </div>
            <div class="card-footer">
                <!-- PAGINATION -->
                <div class="row">
                    <div class="col-sm-4">
                        <p class="text-sm-start fs-6 fw-bold">Halaman {{$gurus->currentPage()}} dari {{$gurus->lastPage()}} | Data {{$gurus->firstItem()}}-{{$gurus->lastItem()}} dari total {{$gurus->total()}}</p>
                    </div>
                    <div class="col-sm-8">
                        <nav class="pagination-admin" aria-label="...">
                            <ul class="pagination d-flex justify-content-end">
                                <li class="page-item {{$gurus->currentPage() == $gurus->onFirstPage() ? 'disabled': ''}}">
                                    <a class="page-link" href="{{$gurus->url($gurus->onFirstPage())}}" id="FirstPage">Previous</a>
                                </li>
                                {{$gurus->appends(Request::all())->onEachSide(1)->links()}}
                                <li class="page-item {{$gurus->currentPage() == $gurus->lastPage() ? 'disabled': ''}}">
                                    <a class="page-link" href="{{$gurus->url($gurus->lastpage())}}" id="LastPage">Last</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- END PAGINATION -->
            </div>
        </div> 
    </div>
    @endsection    

   