
<div class="row g-0">
    <div class="col-sm-3 m-3">
        <img src="{{url($guru->foto)}}" alt="profile" width="200" height="300">
    </div>
    <div class="col-sm-8 ms-3">
        <h5 class="card-title">{{$guru->code}}- {{$guru->nama}}</h5>
        <table class="table table-borderless">
            <tr>
                <td>ID</td>
                <td>{{$guru->code}}</td>
            </tr>
            <tr>
                <td>Nama</td>
                <td>{{$guru->nama}}</td>
            </tr>
            <tr>
                <td>Alamat</td> 
                <td>{{$guru->alamat}}</td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                @if ($guru->jenis_kelamin == 'F')
                <td>{{'Wanita'}}</td>
                @else
                <td>{{'Pria'}}</td>
                @endif
            </tr>
            <tr>
                <td>No Telepon</td>
                <td>{{$guru->no_telepon}}</td>
            </tr>
            <tr>
                <td>Username</td>
                <td>{{$guru->username}}</td>
            </tr>
        </table>
    </div>
</div>    
