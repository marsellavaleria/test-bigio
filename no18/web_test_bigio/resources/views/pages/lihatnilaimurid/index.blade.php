  @extends('layouts.index')

  @section('title','Data Nilai')

  @section('container')
    <div class="container-fluid">
        <h5 class="title-page pt-3">Data Nilai</h5>
        <nav class="breadcrumb-nav" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <p>Data Nilai</p>
                <li class="breadcrumb-item active"><a href="{{route('home.index')}}">Home</a></li>
                <li class="breadcrumb-item" aria-current="page">Data Nilai</li>
            </ol>
        </nav>

        <div class="card show-card">
            <!-- START HEADER -->
            <div class="card-header">
                <div class="row">
                    <div class="col-6 mt-1">
                        <strong> Data Nilai</strong>
                    </div>
                </div>    
            </div>
            <!-- END HEADER -->
            <div class="card-body">
                <table class="table table-borderless">
                    <tr>
                        <td>Nama</td>
                        <td>{{$murid->nama}}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Kelas</td>
                        <td>{{$murid->kelas}}</td>
                        <td></td>
                    </tr>
                </table>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Mata Pelajaran</th>
                            <th scope="col">Nilai</th>   
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($nilaimurids as $nilaimurid)
                        <tr>
                            <th scope="row">{{$loop->iteration+$nilaimurids->firstItem()-1}}</th>
                            <td>{{$nilaimurid->mata_pelajaran}}</td>
                            <td>{{number_format($nilaimurid->nilai, 0, ',', '.')}}</td>
                        <tr>
                        @empty
                        <tr>
                            <td colspan="6" class="text-center p-5">
                                Data tidak tersedia
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                 <!-- PAGINATION -->
                 <div class="row">
                    <div class="col-sm-4">
                        <p class="text-sm-start fs-6 fw-bold">Halaman {{$nilaimurids->currentPage()}} dari {{$nilaimurids->lastPage()}} | Data {{$nilaimurids->firstItem()}}-{{$nilaimurids->lastItem()}} dari total {{$nilaimurids->total()}}</p>
                    </div>
                    <div class="col-sm-8">
                        <nav aria-label="...">
                            <ul class="pagination d-flex justify-content-end">
                                <li class="page-item {{$nilaimurids->currentPage() == $nilaimurids->onFirstPage() ? 'disabled': ''}}">
                                    <a class="page-link" href="{{$nilaimurids->url($nilaimurids->onFirstPage())}}" id="FirstPage">Previous</a>
                                </li>
                                {{$nilaimurids->appends(Request::all())->onEachSide(1)->links()}}
                                <li class="page-item {{$nilaimurids->currentPage() == $nilaimurids->lastPage() ? 'disabled': ''}}">
                                    <a class="page-link" href="{{$nilaimurids->url($nilaimurids->lastpage())}}" id="LastPage">Last</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- END PAGINATION  -->
            </div>
        </div>
    </div>

    
  @endsection
