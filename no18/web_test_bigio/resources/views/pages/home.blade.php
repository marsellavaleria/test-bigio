    @extends('layouts.index')

    @section('title','Home')

    @section('container')
    <div class="container-fluid">
      <h5 class="title-page pt-3">Dashboard</h5>
      <nav class="breadcrumb-nav" aria-label="breadcrumb">
        <ol class="breadcrumb">
          <p>Dashboard</p>
          <li class="breadcrumb-item active" aria-current="page">Home</li>
        </ol>
      </nav>
    </div>
    @endsection

