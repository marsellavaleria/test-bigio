    @include('includes.script')
    <nav class ="sidebar">
        <header>
            @if(Session::get('user')->id_role==1)
            <img src="{{url(Session::get('user')->foto_admin)}}" class="img_slide rounded-circle" width="100px" height="100px" alt="Profile">
            <p> Hi,{{Session::get('user')->nama_admin}}</p>
            @elseif(Session::get('user')->id_role==2)
            <img src="{{url(Session::get('user')->foto_guru)}}" class="img_slide rounded-circle" width="100px" height="100px" alt="Profile">
            <p> Hi,{{Session::get('user')->nama_guru}}</p>
            @else
            <img src="{{url(Session::get('user')->foto_murid)}}" class="img_slide rounded-circle" width="100px" height="100px" alt="Profile">
            <p> Hi,{{Session::get('user')->nama_murid}}</p>
            @endif
        </header>
        <ul>
            <li> <a href="{{route('home.index')}}">Home</a></li>
            @foreach(Session::get('menu') as $menu)
                @if(Session::get('user')->id_role==3 && $menu->id_menu==4)
                <li><a href="{{url("$menu->url",Session::get('user')->id_murid)}}">{{$menu->nama_menu}}</a></li>
                @else
                <li> <a href="{{url("$menu->url")}}">{{$menu->nama_menu}}</a></li>
                @endif
            @endforeach
        </ul>
    </nav>
