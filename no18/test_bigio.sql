-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 03, 2021 at 05:49 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_bigio`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `kode_admin` varchar(255) DEFAULT NULL,
  `nama_admin` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `kode_admin`, `nama_admin`, `foto`) VALUES
(1, 'A00001', 'Admin', '/img_admin/profile.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `jenis_kelamin` varchar(2) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `no_telepon` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `mata_pelajaran` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`id`, `code`, `nama`, `jenis_kelamin`, `alamat`, `no_telepon`, `foto`, `mata_pelajaran`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'G00001', 'Guru', NULL, NULL, NULL, '/img_guru/profile.jpg', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `guruviews`
-- (See below for the actual view)
--
CREATE TABLE `guruviews` (
`id` int(11)
,`code` varchar(255)
,`nama` varchar(255)
,`jenis_kelamin` varchar(2)
,`alamat` varchar(255)
,`no_telepon` varchar(255)
,`foto` varchar(255)
,`mata_pelajaran` varchar(255)
,`iduser` int(11)
,`username` varchar(255)
,`password` varchar(255)
);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id_menu` int(11) NOT NULL,
  `kode_menu` varchar(255) DEFAULT NULL,
  `nama_menu` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id_menu`, `kode_menu`, `nama_menu`, `url`) VALUES
(1, NULL, 'Data Guru', '/guru'),
(2, NULL, 'Data Murid', '/murid'),
(3, NULL, 'Kelola Nilai Murid', '/nilaimurid'),
(4, NULL, 'Lihat Nilai Murid', '/lihatnilaimurid');

-- --------------------------------------------------------

--
-- Table structure for table `murid`
--

CREATE TABLE `murid` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `jenis_kelamin` varchar(2) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `no_telepon` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `kelas` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `murid`
--

INSERT INTO `murid` (`id`, `kode`, `nama`, `jenis_kelamin`, `alamat`, `no_telepon`, `foto`, `kelas`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'M00001', 'Murid', NULL, NULL, NULL, '/img_murid/profile.jpg', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `muridviews`
-- (See below for the actual view)
--
CREATE TABLE `muridviews` (
`id` int(11)
,`kode` varchar(255)
,`nama` varchar(255)
,`jenis_kelamin` varchar(2)
,`alamat` varchar(255)
,`no_telepon` varchar(255)
,`foto` varchar(255)
,`kelas` varchar(255)
,`iduser` int(11)
,`username` varchar(255)
,`password` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `nilaimuridviews`
-- (See below for the actual view)
--
CREATE TABLE `nilaimuridviews` (
`id` int(11)
,`id_murid` int(11)
,`kode` varchar(255)
,`nama` varchar(255)
,`kelas` varchar(255)
,`mata_pelajaran` varchar(255)
,`nilai` decimal(18,4)
);

-- --------------------------------------------------------

--
-- Table structure for table `nilai_murid`
--

CREATE TABLE `nilai_murid` (
  `id` int(11) NOT NULL,
  `mata_pelajaran` varchar(255) DEFAULT NULL,
  `nilai` decimal(18,4) DEFAULT NULL,
  `id_murid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id_role` int(11) NOT NULL,
  `kode_role` varchar(255) DEFAULT NULL,
  `nama_role` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id_role`, `kode_role`, `nama_role`) VALUES
(1, NULL, 'Admin'),
(2, NULL, 'Guru'),
(3, NULL, 'Murid');

-- --------------------------------------------------------

--
-- Stand-in structure for view `roleusermenuviews`
-- (See below for the actual view)
--
CREATE TABLE `roleusermenuviews` (
`id_user` int(11)
,`id_menu` int(11)
,`nama_menu` varchar(255)
,`url` varchar(255)
);

-- --------------------------------------------------------

--
-- Table structure for table `role_menu`
--

CREATE TABLE `role_menu` (
  `id_role_menu` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `role_menu`
--

INSERT INTO `role_menu` (`id_role_menu`, `id_role`, `id_menu`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 3),
(4, 3, 4);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id_role_user` int(11) NOT NULL,
  `id_role` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id_role_user`, `id_role`, `id_user`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `id_admin` int(11) DEFAULT NULL,
  `id_guru` int(11) DEFAULT NULL,
  `id_murid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`iduser`, `username`, `password`, `id_admin`, `id_guru`, `id_murid`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', '$2y$10$PtRlKryELhVeyRmxtYhFTeMOgETnmj0VU8pRhYr12H3Vrdqiuet0S', 1, NULL, NULL, NULL, NULL, NULL),
(2, 'guru', '$2y$10$bV4rGnbZabU6ECKZvoKxA.mpzMl51MTD1B2lJ/YXHnBUhrE.0RaAa', NULL, 1, NULL, NULL, NULL, NULL),
(3, 'murid', '$2y$10$j4iW/fz64FkOrcqgRlHAke073bq2nav5WQmZNYKji51nrQidEMDVK', NULL, NULL, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `userviews`
-- (See below for the actual view)
--
CREATE TABLE `userviews` (
`iduser` int(11)
,`username` varchar(255)
,`password` varchar(255)
,`id_admin` int(11)
,`nama_admin` varchar(255)
,`foto_admin` varchar(255)
,`id_guru` int(11)
,`nama_guru` varchar(255)
,`foto_guru` varchar(255)
,`id_murid` int(11)
,`nama_murid` varchar(255)
,`foto_murid` varchar(255)
,`id_role` int(11)
,`nama_role` varchar(255)
);

-- --------------------------------------------------------

--
-- Structure for view `guruviews`
--
DROP TABLE IF EXISTS `guruviews`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `guruviews`  AS SELECT `g`.`id` AS `id`, `g`.`code` AS `code`, `g`.`nama` AS `nama`, `g`.`jenis_kelamin` AS `jenis_kelamin`, `g`.`alamat` AS `alamat`, `g`.`no_telepon` AS `no_telepon`, `g`.`foto` AS `foto`, `g`.`mata_pelajaran` AS `mata_pelajaran`, `u`.`iduser` AS `iduser`, `u`.`username` AS `username`, `u`.`password` AS `password` FROM (`guru` `g` left join `user` `u` on(`u`.`id_guru` = `g`.`id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `muridviews`
--
DROP TABLE IF EXISTS `muridviews`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `muridviews`  AS SELECT `m`.`id` AS `id`, `m`.`kode` AS `kode`, `m`.`nama` AS `nama`, `m`.`jenis_kelamin` AS `jenis_kelamin`, `m`.`alamat` AS `alamat`, `m`.`no_telepon` AS `no_telepon`, `m`.`foto` AS `foto`, `m`.`kelas` AS `kelas`, `u`.`iduser` AS `iduser`, `u`.`username` AS `username`, `u`.`password` AS `password` FROM (`murid` `m` left join `user` `u` on(`u`.`id_murid` = `m`.`id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `nilaimuridviews`
--
DROP TABLE IF EXISTS `nilaimuridviews`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `nilaimuridviews`  AS SELECT `nimu`.`id` AS `id`, `m`.`id` AS `id_murid`, `m`.`kode` AS `kode`, `m`.`nama` AS `nama`, `m`.`kelas` AS `kelas`, `nimu`.`mata_pelajaran` AS `mata_pelajaran`, `nimu`.`nilai` AS `nilai` FROM (`nilai_murid` `nimu` join `murid` `m` on(`nimu`.`id_murid` = `m`.`id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `roleusermenuviews`
--
DROP TABLE IF EXISTS `roleusermenuviews`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `roleusermenuviews`  AS SELECT `ro_us`.`id_user` AS `id_user`, `me`.`id_menu` AS `id_menu`, `me`.`nama_menu` AS `nama_menu`, `me`.`url` AS `url` FROM ((`role_user` `ro_us` join `role_menu` `ro_me` on(`ro_us`.`id_role` = `ro_me`.`id_role`)) join `menu` `me` on(`me`.`id_menu` = `ro_me`.`id_menu`)) ;

-- --------------------------------------------------------

--
-- Structure for view `userviews`
--
DROP TABLE IF EXISTS `userviews`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `userviews`  AS SELECT `u`.`iduser` AS `iduser`, `u`.`username` AS `username`, `u`.`password` AS `password`, `u`.`id_admin` AS `id_admin`, `a`.`nama_admin` AS `nama_admin`, `a`.`foto` AS `foto_admin`, `u`.`id_guru` AS `id_guru`, `g`.`nama` AS `nama_guru`, `g`.`foto` AS `foto_guru`, `u`.`id_murid` AS `id_murid`, `m`.`nama` AS `nama_murid`, `m`.`foto` AS `foto_murid`, `r`.`id_role` AS `id_role`, `r`.`nama_role` AS `nama_role` FROM (((((`user` `u` left join `admin` `a` on(`a`.`id_admin` = `u`.`id_admin`)) left join `guru` `g` on(`g`.`id` = `u`.`id_guru`)) left join `murid` `m` on(`m`.`id` = `u`.`id_murid`)) left join `role_user` `ru` on(`ru`.`id_user` = `u`.`iduser`)) left join `role` `r` on(`r`.`id_role` = `ru`.`id_role`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `murid`
--
ALTER TABLE `murid`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nilai_murid`
--
ALTER TABLE `nilai_murid`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indexes for table `role_menu`
--
ALTER TABLE `role_menu`
  ADD PRIMARY KEY (`id_role_menu`),
  ADD KEY `role_menu_role` (`id_role`),
  ADD KEY `role_menu_menu` (`id_menu`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id_role_user`),
  ADD KEY `role_user_role` (`id_role`),
  ADD KEY `role_user_user` (`id_user`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `murid`
--
ALTER TABLE `murid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nilai_murid`
--
ALTER TABLE `nilai_murid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `role_menu`
--
ALTER TABLE `role_menu`
  MODIFY `id_role_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id_role_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `role_menu`
--
ALTER TABLE `role_menu`
  ADD CONSTRAINT `role_menu_menu` FOREIGN KEY (`id_menu`) REFERENCES `menu` (`id_menu`),
  ADD CONSTRAINT `role_menu_role` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`);

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
